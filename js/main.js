$(document).ready(function() {
  var logo = $('#invert');

  logo.on('click touch', function() {
    logo.toggleClass('inverted');
    $('body').toggleClass('inverted');
  });
});
